<?php
/*
Plugin Name: Robots meta tag modifier
Description: Disables meta robots indexing (noindex) for 2008 year posts
Version: 0.0.1
Author: Denis Nagorny
*/

add_action('get_header', 'ss_robots_modifier_init');
function ss_robots_modifier_init() {
	global $post;
	if (is_single() && (get_the_time('Y', $post->ID) == '2008')) {
		ob_start();
		add_action('wp_head', 'ss_robots_modifier', 20);
	}
}
function ss_robots_modifier() {
	$wp_head = ob_get_contents();
	$wp_head = preg_replace('#<meta name=[\'"]robots[\'"].*?>#i', '', $wp_head);
	$wp_head .= PHP_EOL.'	<meta name="robots" content="noindex" />'.PHP_EOL;
	ob_end_clean();
	echo $wp_head;
}



